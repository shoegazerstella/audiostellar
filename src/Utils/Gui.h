#pragma once

#include "ofMain.h"
#include "ofxImGui.h"
#include "Sounds.h"
#include "MidiServer.h"
#include "Modes.h"
#include "SessionManager.h"
#include "AudioDimensionalityReduction.h"
#include "GuiTheme.h"
#include "ofxJSON.h"
#include "Tooltip.h"
#include "OscServer.h"
#include "Tutorial.h"

#define WINDOW_TOOLS_WIDTH 350
#define WINDOW_TOOLS_HEIGHT 600

class SessionManager;

class Gui {

private:
    static Gui* instance;
    Gui(MidiServer * midiServer,
        Modes * modes,
        SessionManager * sessionManager,
        OscServer * oscServer);

    AudioDimensionalityReduction adr;
    bool drawGui = true;

    #ifndef TARGET_OS_MAC
    bool TARGET_OS_MAC = false;
    #endif

    SessionManager * sessionManager;
    Sounds * sounds;
    MidiServer * midiServer;
    OscServer * oscServer;
    Modes * modes;
    Tooltip * tooltip;

    ofxJSONElement jsonFile;

    bool isToolsWindowHovered = false;
    bool isMainMenuHovered = false;
    bool isAboutScreenHovered = false;
    bool isWelcomeScreenHovered = false;
    bool isDimReductScreenHovered = false;
    bool isProcessingFilesWindowHovered = false;
    bool isMessageWindowHovered = false;
    bool isTutorialWindowHovered = false;

    bool isWelcomeScreenOpen = false;
    bool isAboutScreenOpen = true;
    bool isDimReductScreenOpen = true;
    bool isTutorialOpen = false;
    
    bool haveToDrawAboutScreen = false;
    bool haveToDrawDimReductScreen = false;
    bool haveToDrawMessage = false;
    bool haveToDrawMessageCloseButton = false;
    bool haveToDrawFPS = false;
    bool haveToHideCursor = false;
    bool toggleFullscreen = false;
    bool isFullscreen = false;
    
    string messageTitle = "";
    string messageDescription = "";

//    This should be implemented in 0.9.1
//
//    const char* sample_rate_items[4] = { "11025", "22050", "44100", "48000" };
//    int sample_rate_values[4] = { 11025, 22050, 44100, 48000 };
//    int sample_rate_item_current = 1;
    
//    const char* window_size_items[5] = { "512", "1024", "2048", "4096", "8192" };
//    int window_size_values[5] = { 512, 1024, 2048, 4096, 8192 };
//    int window_size_item_current = 2;
    
//    const char* hop_size_items[5] = { "Window size ", "Window size / 2", "Window size / 4", "Window size / 8", "Window size / 16" };
//    int hop_size_values[5] = { 1, 2, 4, 8, 16 };
//    int hop_size_item_current = 2;

    const char* features_items[2] = { "stft", "mfcc" };
    int features_item_current = 0;
    
    const string welcomeScreenText =
            "Commander, the default session have been loaded. \n\n"
            "Are you up for a fast tutorial to get you started ? \n\n";

    bool isProcessingFiles = false;

    void drawDimReductScreen();
    void drawProcessingFilesWindow();

    void drawWelcomeScreen();
    void drawMainMenu();
    void drawAboutScreen();
    void drawFPS();
    void drawMessage();
    void drawTutorial();
    void drawContextMenu();

    ofTrueTypeFont font;
    Tutorial tutorial;
    Tutorial::TutorialPage * tutorialCurrentPage = nullptr;
    
    ImFont* font1;
public:

    static Gui* getInstance();
    static Gui* getInstance(MidiServer * midiServer,
                            Modes * modes,
                            SessionManager * sessionManager,
                            OscServer * oscServer);

    ofxImGui::Gui gui;

    void newSession();

    void draw();
    void keyPressed(ofKeyEventArgs & e);

    bool isMouseHoveringGUI();

    void showMessage(string description, string title = "", bool showCloseButton = true);
    void hideMessage();

    void showWelcomeScreen();
    void startTutorial();
};
