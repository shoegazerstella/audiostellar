#include "DBScan.h"

DBScan::DBScan(double eps, int minPts, vector<ofPoint> points)
{
    this->eps = eps;
    this->minPts = minPts;
    this->size = (int)points.size();
    adjPoints.resize(size);
    this->clusterIdx=-1;

    for ( int i = 0 ; i < points.size() ; i++ ) {
        ofPoint point = points[i];
        dbscanPoint dPoint;

        dPoint.x = point.x;
        dPoint.y = point.y;

        dPoint.ptsCnt = 0;
        dPoint.cluster = NOT_CLASSIFIED;

        this->points.push_back(dPoint);
    }
}

void DBScan::run()
{
    checkNearPoints();

    for(int i=0;i<size;i++) {
        if(points[i].cluster != NOT_CLASSIFIED) continue;

        if(isCoreObject(i)) {
            dfs(i, ++clusterIdx);
        } else {
            points[i].cluster = NOISE;
        }
    }

    cluster.resize(clusterIdx+1);

    for(int i=0;i<size;i++) {
        if(points[i].cluster != NOISE) {
            cluster[points[i].cluster].push_back(i);
        }
    }
}

vector<vector<int> > DBScan::getCluster()
{
    return cluster;
}

void DBScan::dfs(int now, int c)
{
    points[now].cluster = c;
    if(!isCoreObject(now)) return;

    for(auto&next:adjPoints[now]) {
        if(points[next].cluster != NOT_CLASSIFIED) continue;
        dfs(next, c);
    }
}

void DBScan::checkNearPoints()
{
    for(int i=0;i<size;i++) {
        for(int j=0;j<size;j++) {
            if(i==j) continue;

            if(points[i].getDis(points[j]) <= eps) {
                points[i].ptsCnt++;
                adjPoints[i].push_back(j);
            }
        }
    }
}

bool DBScan::isCoreObject(int idx)
{
    return points[idx].ptsCnt >= minPts;
}
