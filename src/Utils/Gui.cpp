#include "Gui.h"

Gui* Gui::instance = nullptr;

Gui::Gui(MidiServer * midiServer,
         Modes * modes,
         SessionManager * sessionManager,
         OscServer *oscServer) {

    this->sounds = Sounds::getInstance();
    this->midiServer = midiServer;
    this->oscServer = oscServer;
    this->modes = modes;
    this->sessionManager = sessionManager;
    this->tooltip = new Tooltip();
    
    font.load( OF_TTF_MONO, 10 );
    
    gui.setup(new GuiTheme(), true);
    drawGui = true;
}

Gui* Gui::getInstance(){
    /* Esto puede fallar??
    if(instance == 0){
        instance = new Gui();
    }
    */
    return instance;
}

Gui* Gui::getInstance(MidiServer * midiServer,
                      Modes * modes,
                      SessionManager * sessionManager,
                      OscServer * oscServer)
{
    if(instance == nullptr){
        instance = new Gui(midiServer, modes, sessionManager, oscServer);
    }
    return instance;
}


void Gui::newSession()
{
    haveToDrawDimReductScreen = true;
    isDimReductScreenOpen = true;
}

void Gui::drawProcessingFilesWindow()
{
    if ( isProcessingFiles ) {
        adr.checkProcess();

        if ( adr.progress == 1.0f ) {
            isProcessingFiles = false;

            string resultJSONpath = AudioDimensionalityReduction::generatedJSONPath;
            if ( resultJSONpath != "" ) {
                //sessionManager->loadSession(resultJSONpath.c_str());
                SessionManager::datasetLoadOptions opts;
                opts.method = "RECENT";
                opts.path = resultJSONpath.c_str();
                sessionManager->loadSession(opts);

                adr.dirPath = "";

                string strSuccess = "Adjust clustering parameters using the Settings menu.\n";

                if ( !adr.failFiles.empty() ) {
                    strSuccess += "\n:( Some files couldn't be processed: ";
                    for ( auto s : adr.failFiles ) {
                        strSuccess += "\n" + s;
                    }
                }

                showMessage( strSuccess, "Process ended successfully" );
            } else {
                showMessage( "Unexpected error", "Error" );
            }
            AudioDimensionalityReduction::end();
        } else if ( adr.progress == -1.0f ) {
            isProcessingFiles = false;
            showMessage( adr.currentProcess, "Error" );
            AudioDimensionalityReduction::end();
        } else {
            ImGuiWindowFlags window_flags = 0;
            window_flags |= ImGuiWindowFlags_NoScrollbar;
            window_flags |= ImGuiWindowFlags_NoMove;
            window_flags |= ImGuiWindowFlags_NoResize;
            window_flags |= ImGuiWindowFlags_NoCollapse;
            window_flags |= ImGuiWindowFlags_NoNav;
            window_flags |= ImGuiWindowFlags_AlwaysAutoResize;
            ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - 200 ,100));

            bool isWindowOpen = true;

            string windowTitle = "Processing " +
                    ofToString(adr.filesFound) +
                    " files...";

            if ( ImGui::Begin( windowTitle.c_str(), &isWindowOpen, window_flags)) {
                
                isProcessingFilesWindowHovered = ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem | ImGuiHoveredFlags_AllowWhenBlockedByPopup | ImGuiHoveredFlags_ChildWindows);
                
                ImGui::Text("Grab a coffee, "
                            "this can take quite a while.");

                #ifndef TARGET_OS_WINDOWS
                ImGui::ProgressBar( adr.progress );
                #else
                ImGui::Text( ofToString(adr.progress).c_str() );
                #endif

                ImGui::Text( adr.currentProcess.c_str() );
            }
            ImGui::End();
        }
    }
}

void Gui::draw() {

    gui.begin();

    //Menu Superior
    drawMainMenu();

    if(drawGui) {

        //Botones de seleccion de modo
        modes->drawModeSelector();

        //Tools window
        ImGuiWindowFlags window_flags = 0;
        window_flags |= ImGuiWindowFlags_NoScrollbar;
        window_flags |= ImGuiWindowFlags_NoMove;
        window_flags |= ImGuiWindowFlags_NoResize;
        window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_NoNav;
        window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

        auto mainSettings = ofxImGui::Settings();
        mainSettings.windowPos = ImVec2(ofGetWidth() - WINDOW_TOOLS_WIDTH - 20, 30);
        mainSettings.lockPosition = true;

        if ( isToolsWindowHovered )
            ImGui::PushStyleVar(ImGuiStyleVar_Alpha, 1.f);
        else
            ImGui::PushStyleVar(ImGuiStyleVar_Alpha, .7f);
        if( ofxImGui::BeginWindow("Tools", mainSettings, window_flags)){
                ImGui::SetWindowSize( ImVec2(WINDOW_TOOLS_WIDTH,WINDOW_TOOLS_HEIGHT) );
                isToolsWindowHovered = ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem
                                                              | ImGuiHoveredFlags_RootAndChildWindows);

                midiServer->SliderFloat("Master Volume", &sounds->volumen, 0.0f, 1.0f);
                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::MASTER_VOLUME);
            
                ImGui::Checkbox("Midi Learn", &midiServer->midiLearn);
                modes->drawModesSettings();
        }

        ofxImGui::EndWindow(mainSettings);
        ImGui::PopStyleVar();

        Tooltip::drawGui();


    }

    drawProcessingFilesWindow();
    drawAboutScreen();
    drawDimReductScreen();
    drawTutorial();

    haveToHideCursor ? ofHideCursor() : ofShowCursor();

    if(toggleFullscreen && !isFullscreen || !toggleFullscreen && isFullscreen){
        ofToggleFullscreen();
        isFullscreen = !isFullscreen;
    }

    drawWelcomeScreen();

    drawFPS();
    drawMessage();

    drawContextMenu();

    gui.end();
}

void Gui::drawWelcomeScreen(){

   if(isWelcomeScreenOpen){
       
       ImGuiWindowFlags window_flags = 0;
       int w = 400;
       window_flags |= ImGuiWindowFlags_NoScrollbar;
       window_flags |= ImGuiWindowFlags_NoMove;
       window_flags |= ImGuiWindowFlags_NoResize;
       window_flags |= ImGuiWindowFlags_NoCollapse;
       window_flags |= ImGuiWindowFlags_NoNav;
       window_flags |= ImGuiWindowFlags_AlwaysAutoResize;
       
       ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - w/2, 100));

       ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding,ImVec2(20,20));

       if (ImGui::Begin("Welcome to AudioStellar" , &isWelcomeScreenOpen, window_flags)){
           
           isWelcomeScreenHovered = ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem | ImGuiHoveredFlags_AllowWhenBlockedByPopup | ImGuiHoveredFlags_ChildWindows);

           ImGui::PushTextWrapPos(w);
           ImGui::TextWrapped(welcomeScreenText.c_str());
           ImGui::Columns(2,NULL, false);

           if(ImGui::Button("Yes, please.")) {
              isWelcomeScreenOpen = false;
              isWelcomeScreenHovered = false;
              startTutorial();
           }

           ImGui::NextColumn();

           if(ImGui::Button("No, I can handle this.")){
               isWelcomeScreenOpen = false;
               isWelcomeScreenHovered = false;
           }

           ImGui::End();
       }

       ImGui::PopStyleVar();
   } else {
       isWelcomeScreenHovered = false;
   }

}

void Gui::drawMainMenu(){
    if(ImGui::BeginMainMenuBar()){
        
        isMainMenuHovered = ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem
                                                   | ImGuiHoveredFlags_AllowWhenBlockedByPopup
                                                   | ImGuiHoveredFlags_ChildWindows);

        if(ImGui::BeginMenu("File")){

            if ( ImGui::MenuItem( "New", TARGET_OS_MAC ? "Cmd+N" : "Ctrl+N") ) {
                newSession();
            }

            if(ImGui::MenuItem("Open", TARGET_OS_MAC ? "Cmd+O" : "Ctrl+O")){
                SessionManager::datasetLoadOptions opts;
                opts.method = "GUI";
                sessionManager->loadSession(opts);
            }

            if(ImGui::MenuItem("Save", TARGET_OS_MAC ? "Cmd+S" : "Ctrl+S")){
                sessionManager->saveSession();
            }

            if(ImGui::MenuItem("Save copy as...", TARGET_OS_MAC ? "Cmd+Shift+S" : "Ctrl+Sift+S")){
                sessionManager->saveAsNewSession();
            }

            if(ImGui::BeginMenu("Recent Projects")){
                vector<string> recentProjects = sessionManager->getRecentProjects();
                if(sessionManager->areAnyRecentProjects(recentProjects)){
                    for(unsigned int i = 0; i < recentProjects.size(); i++){
                        if(ImGui::MenuItem(recentProjects[i].c_str())){
                            //sessionManager->loadSession(recentProjects[i].c_str());
                            SessionManager::datasetLoadOptions opts;
                            opts.method = "RECENT";
                            opts.path = recentProjects[i].c_str();
                            sessionManager->loadSession(opts);
                        }
                    }
                }else{
                    ImGui::PushStyleColor(ImGuiCol_Text,ImVec4(0.5,0.5,0.5,1.0));
                    ImGui::MenuItem("No recent Projects...");
                    ImGui::PopStyleColor();
                }
                ImGui::EndMenu();
            }

            if(ImGui::MenuItem("Quit", TARGET_OS_MAC ? "Cmd+Q" : "Ctrl+Q")){
                exit(0);
            }

            ImGui::EndMenu();
        }

        if(ImGui::BeginMenu("View")){
            ImGui::Checkbox("Show UI", &drawGui);
            ImGui::Checkbox("Show sound filenames", &sounds->showSoundFilenamesTooltip);
            ImGui::Checkbox("Show FPS", &haveToDrawFPS);
            ImGui::Checkbox("Hide Cursor", &haveToHideCursor);
            ImGui::Checkbox("Fullscreen", &toggleFullscreen);
            ImGui::Checkbox("Show Tooltips", &Tooltip::enabled);
            ImGui::EndMenu();
        }

        if(ImGui::BeginMenu("Settings")){
           sounds->drawGui();
           midiServer->drawGui();
           oscServer->drawGui();
           ImGui::EndMenu();
        }

        if(ImGui::BeginMenu("Help")){
            if(ImGui::MenuItem("About")){
                haveToDrawAboutScreen = true;
                isAboutScreenOpen = true;
            }
            if(ImGui::MenuItem("Tutorial")){
                startTutorial();
            }
           ImGui::EndMenu();
        }
        ImGui::EndMainMenuBar();
    }
}

void Gui::drawAboutScreen(){

    if(haveToDrawAboutScreen) {
        if(isAboutScreenOpen){
            
            ImGuiWindowFlags window_flags = 0;
            window_flags |= ImGuiWindowFlags_NoScrollbar;
            window_flags |= ImGuiWindowFlags_NoMove;
            window_flags |= ImGuiWindowFlags_NoResize;
            window_flags |= ImGuiWindowFlags_NoCollapse;
            window_flags |= ImGuiWindowFlags_NoNav;
            window_flags |= ImGuiWindowFlags_AlwaysAutoResize;
            
            ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - 200, ofGetHeight()/3));
            if(ImGui::Begin("About", &isAboutScreenOpen, window_flags)){
                
                isAboutScreenHovered = ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem | ImGuiHoveredFlags_AllowWhenBlockedByPopup | ImGuiHoveredFlags_ChildWindows);
                
               ImGui::Text("Thanks for using AudioStellar v0.9.0. \n\n\nMore info at http://www.audiostellar.xyz");
            }
            ImGui::End();
        }else{
            haveToDrawAboutScreen = false;
            isAboutScreenHovered = false;
        }
    }
}

void Gui::drawDimReductScreen(){
    
    if(haveToDrawDimReductScreen) {
        
        if(isDimReductScreenOpen){
            
            ImGuiWindowFlags window_flags = 0;
            int w = 400;
            window_flags |= ImGuiWindowFlags_NoScrollbar;
            window_flags |= ImGuiWindowFlags_NoMove;
            window_flags |= ImGuiWindowFlags_NoResize;
            window_flags |= ImGuiWindowFlags_NoCollapse;
            window_flags |= ImGuiWindowFlags_NoNav;
            window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

            ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - w/2, 100));
            ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding,ImVec2(20,20));
            
            if (ImGui::Begin("Generate new session" , &isDimReductScreenOpen, window_flags)){
                
                isDimReductScreenHovered = ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem | ImGuiHoveredFlags_AllowWhenBlockedByPopup | ImGuiHoveredFlags_ChildWindows);
                
                //path
                ImGui::Text("Folder location");
                
                string folder;
                string containingFolder;
                string displayFolder;
                
                if(adr.dirPath.size()) {
                    folder = ofFilePath::getPathForDirectory(adr.dirPath);
                    displayFolder = folder;
                }
                
                ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0,3));
                ImGui::BeginChild("child", ImVec2(208, 19), true);
                ImGui::TextUnformatted( displayFolder.c_str() );
                ImGui::EndChild();
                ImGui::PopStyleVar();
                
                ImGui::SameLine();
                if(ImGui::Button("Select folder")){
                    ofFileDialogResult dialogResult = ofSystemLoadDialog("Select Folder", true, ofFilePath::getUserHomeDir());
                    string path;

                    if(dialogResult.bSuccess) {
                        adr.dirPath = dialogResult.getPath();

                    }
                }
                ImGui::Text("");
                
//                ImGui::Text("Audio");
//                ImGui::Combo("Sample rate", &sample_rate_item_current, sample_rate_items, IM_ARRAYSIZE(sample_rate_items));
//                adr.targetSampleRate = sample_rate_values[sample_rate_item_current];
//                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_AUDIO_SAMPLE_RATE);
                
                ImGui::InputFloat("Target audio length", &adr.targetDuration);
                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_AUDIO_LENGTH);

                ImGui::Combo("Audio feature extraction", &features_item_current, features_items, IM_ARRAYSIZE(features_items));
                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_AUDIO_FEATURES);
                
//                ImGui::Text("STFT");
//                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_STFT);
//                ImGui::Combo("Window size", &window_size_item_current, window_size_items, IM_ARRAYSIZE(window_size_items));
//                adr.stft_windowSize = window_size_values[window_size_item_current];
//                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_STFT_WINDOW_SIZE);
                
//                ImGui::Combo("Hop size", &hop_size_item_current, hop_size_items, IM_ARRAYSIZE(hop_size_items));
//                adr.stft_hopSize = window_size_values[window_size_item_current] / hop_size_values[hop_size_item_current];
//                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_STFT_HOP_SIZE);
//                ImGui::Text("");
                
//                ImGui::Text("PCA");
//                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_PCA);
//                ImGui::InputInt("PCA components", &adr.pca_nComponents);
//                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_PCA_COMPONENTS);
//                ImGui::Text("");
                
//                ImGui::Text("T-SNE");
//                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_TSNE);
//                ImGui::InputDouble("Perplexity", &adr.tsne_perplexity);
//                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_TSNE_PERPLEXITY);
//                ImGui::InputDouble("Theta", &adr.tsne_theta);
//                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_TSNE_THETA);
//                ImGui::Text("");
                
                
                if(adr.dirPath.size()) {
                    if(ImGui::Button("Run")) {
                        adr.audioFeatures = features_items[features_item_current];

                        haveToDrawDimReductScreen = false;
                        isDimReductScreenHovered = false;
                        isProcessingFiles = true;
                        isWelcomeScreenOpen = false;
                        adr.startThread();
                    }
                }else{
                    ImVec4 color = ImColor(0.3f, 0.3f, 0.3f, 0.5f);
                    ImVec4 textColor = ImColor(1.f, 1.0f, 1.f, 0.5f);
                    ImGui::PushStyleColor(ImGuiCol_Text, textColor);
                    ImGui::PushStyleColor(ImGuiCol_Button, color);
                    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, color);
                    ImGui::PushStyleColor(ImGuiCol_ButtonActive, color);
                    ImGui::Button("Run");
                    ImGui::PopStyleColor(4);
                }
                ImGui::End();
            }
            ImGui::PopStyleVar();
            
        }else {
            isDimReductScreenHovered = false;
        }
    }
}

void Gui::drawFPS()
{
    if ( haveToDrawFPS ) {
        ofSetColor(255);
        font.drawString(
            "FPS: " + ofToString(ofGetFrameRate(), 0),
            ofGetWidth() - 70,
            ofGetHeight() - 10);
    }
}

void Gui::drawMessage()
{
    if ( haveToDrawMessage ) {
        ImGuiWindowFlags window_flags = 0;
        window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_NoResize;

        int w = 350;
        int h = 100;

        ImGui::SetNextWindowSize( ImVec2(w,h) );
        ImGui::SetNextWindowPos(ImVec2( (ofGetWidth()/2) - w/2, (ofGetHeight()/2) - h/2 ));

        if(ImGui::Begin( messageTitle.c_str() , &haveToDrawMessage, window_flags)) {
            
            isMessageWindowHovered = ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem | ImGuiHoveredFlags_AllowWhenBlockedByPopup | ImGuiHoveredFlags_ChildWindows);
            
           ImGui::PushTextWrapPos(w);
           ImGui::Text(messageDescription.c_str());
        }
        ImGui::End();
    } else {
        isMessageWindowHovered = false;
    }
}

void Gui::drawTutorial()
{
    ImGuiWindowFlags window_flags = 0;
    int w = 400;
    window_flags |= ImGuiWindowFlags_NoScrollbar;
    window_flags |= ImGuiWindowFlags_NoResize;
    window_flags |= ImGuiWindowFlags_NoCollapse;
    window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

//    ImGui::SetNextWindowPos(ImVec2(100, 100));

   if(isTutorialOpen){

       ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding,ImVec2(20,20));

       if (ImGui::Begin(tutorialCurrentPage->title.c_str() , &isTutorialOpen, window_flags)){
           isTutorialWindowHovered = ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem | ImGuiHoveredFlags_AllowWhenBlockedByPopup | ImGuiHoveredFlags_ChildWindows);

           ImGui::PushTextWrapPos(w);
           ImGui::TextWrapped(tutorialCurrentPage->content.c_str());

           string buttonTitle = "Next";
           if ( tutorial.isLastPage() ) {
               buttonTitle = "Finish";
           }

           if(ImGui::Button(buttonTitle.c_str())){
                tutorialCurrentPage = tutorial.getNextPage();
                if ( tutorialCurrentPage == nullptr ) {
                    isTutorialOpen = false;
                    isTutorialWindowHovered = false;
                }
           }

           ImGui::End();
       }

       ImGui::PopStyleVar();
   } else {
       isTutorialWindowHovered = false;
   }
}

void Gui::drawContextMenu(){
    sounds->drawClusterTagger();
}

void Gui::showMessage(string description, string title, bool showCloseButton)
{
    haveToDrawMessage = true;
    haveToDrawMessageCloseButton = showCloseButton;
    messageTitle = title;
    messageDescription = description;
}

void Gui::hideMessage()
{
    haveToDrawMessage = false;
}

void Gui::showWelcomeScreen()
{
    isWelcomeScreenOpen = true;
}

void Gui::startTutorial()
{
    tutorialCurrentPage = tutorial.start();
    isTutorialOpen = true;
}

void Gui::keyPressed(ofKeyEventArgs & e) {
    
    if(e.key == 'g') {
        drawGui = !drawGui;
    }
    
    #ifdef TARGET_OS_MAC
    if ( e.key == 'n' && e.hasModifier(OF_KEY_COMMAND) ) newSession();
    #else
    if ( e.key == 'n' && e.hasModifier(OF_KEY_CONTROL) ) newSession();
    #endif
    
}

bool Gui::isMouseHoveringGUI() {

    
    return ( isToolsWindowHovered
             || isMainMenuHovered
             || isAboutScreenHovered
             || isWelcomeScreenHovered
             || isDimReductScreenHovered
             || isProcessingFilesWindowHovered
             || isMessageWindowHovered
             || isTutorialWindowHovered
             || modes->isMouseHoveringGui() );
}

