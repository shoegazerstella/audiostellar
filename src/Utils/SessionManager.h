#pragma once

#include "ofMain.h"
#include "Utils.h"
#include "ofxJSON.h"
#include "Modes/Modes.h"
#include "Sound/Sounds.h"
#include "Utils/MidiServer.h"
#include "ofxImGui.h"
#include "OscServer.h"
#include "Gui.h"


class SessionManager {

private:

    Modes * modes = NULL;
    Sounds * sounds = NULL;
    MidiServer * midiServer = NULL;

    const string STRING_ERROR = "ERROR";

    //SESSION
    struct datasetResult{
        bool success;
        string status;
        ofxJSONElement data;
    };

    string jsonFilename = "";
    ofxJSONElement jsonFile;

    string userSelectJson();
    datasetResult validateDataset(string datasetPath);

    bool sessionLoaded = false;

    //STELLAR FILE
    const string SETTINGS_FILENAME = "stellar.json";
    ofxJSONElement settingsFile;
    bool settingsFileExist();
    void createSettingsFile();
    void loadSettings();
    void saveSettingsFile();
   

    //RECENT PROJECTS
    ofFile recentProjectsFile;
    vector<string> recentProjects;
    vector<string> loadRecentProjects();
    void saveToRecentProject(string path);
    const string DEFAULT_SESSION = "default.session.json";

    //LAST MIDI DEV
    string lastSavedMidiDevice = "";

    void setWindowTitleWithSessionName();

public:

    SessionManager(Modes * modes,
                   MidiServer * midiServer);


    struct datasetLoadOptions{
        string method;
        string path;
    };

    void loadSession(datasetLoadOptions opts);
    void loadDefaultSession();
    void saveSession();

    void saveAsNewSession();
    void exit();

    bool areAnyRecentProjects(vector<string> recentProjects);

    //Getters
    bool getSessionLoaded();
    vector<string> getRecentProjects();

    void keyPressed(ofKeyEventArgs & e);
};
