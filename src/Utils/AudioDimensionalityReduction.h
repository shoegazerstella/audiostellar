#pragma once
#include "ofMain.h"
#include "ofxJSON.h"

#define FILENAME_PROCESS_INFO "processInfo.json"

class AudioDimensionalityReduction : public ofThread
{
public:
    static string currentProcess;
    static float progress;
    static int filesFound;
    static string generatedJSONPath;
    static vector<string> failFiles;

    string dirPath;

    int targetSampleRate = 22050;
    float targetDuration = 3.0; // Seconds. Audios exceeding duration will be chopped, otherwise will be zero padded
    int stft_windowSize = 2048;
    int stft_hopSize = 2048/4;
    int pca_nComponents = 300;
    double tsne_perplexity = 30;
    double tsne_theta = 0.5;
    string audioFeatures = "stft";
    
    /**
     * @brief run Runs the entire pipeline. Audio files inside dirPath
     * should be at least of audioSampleRate (downsampling will be
     * performed)
     * @param dirPath
     * @param audioSampleRate
     * @param audioDuration
     * @param stft_windowSize
     * @param stft_hopSize
     * @param pca_nComponents
     * @param tsne_perplexity
     * @param tsne_theta
     * @return string path of created json file or ""
     */
    static void run( string dirPath,
                      int targetSampleRate,
                      float targetDuration,
                      string audioFeatures,
                      int stft_windowSize,
                      int stft_hopSize,
                      int pca_nComponents,
                      double tsne_perplexity,
                      double tsne_theta);
    
    static void checkProcess();
    static void end();

    void threadedFunction();
    
private:

};
