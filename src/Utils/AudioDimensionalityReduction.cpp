#include "AudioDimensionalityReduction.h"
string AudioDimensionalityReduction::currentProcess = "";

float AudioDimensionalityReduction::progress = 0;
int AudioDimensionalityReduction::filesFound = 0;
string AudioDimensionalityReduction::generatedJSONPath = "";
vector<string> AudioDimensionalityReduction::failFiles;

void AudioDimensionalityReduction::run(string dirPath,
                                         int targetSampleRate,
                                         float targetDuration,
                                         string audioFeatures,
                                         int stft_windowSize,
                                         int stft_hopSize,
                                         int pca_nComponents,
                                         double tsne_perplexity,
                                         double tsne_theta) {
    
    string dataAnalysisFilename = "audiostellar-data-analysis";
    #ifdef TARGET_OS_WINDOWS
    dataAnalysisFilename = "audiostellar-data-analysis.exe";
    #endif

    if (dirPath.size() > 0) {
        ofFilePath ofp;
        string binPath = ofp.join( ofp.getCurrentExeDir(), dataAnalysisFilename);

        #ifdef TARGET_OS_WINDOWS
           binPath = "\"" + binPath + "\"";
           dirPath = "\"" + dirPath + "\"";
        #endif

        string command = binPath + " "
                         + dirPath + " "
                         + "-f " + audioFeatures + " "
                         + "-l " + ofToString(targetDuration);


        ofLog() << "running " << command;
        ofLog() << ofSystem( command );
    }
}

void AudioDimensionalityReduction::checkProcess()
{
    ofxJSONElement file;
    ofFilePath ofp;
    string processInfoPath;
    
    #ifdef TARGET_OS_MAC
    processInfoPath = ofp.join( ofp.getCurrentExeDir() + "../Resources", FILENAME_PROCESS_INFO );
    #else
    processInfoPath = ofp.join( ofp.getCurrentExeDir(), FILENAME_PROCESS_INFO);
    #endif

    if ( file.open( processInfoPath ) ) {
        Json::Value jsonQtyFiles = file["qtyFiles"];
        Json::Value jsonProgress = file["progress"];
        Json::Value jsonCurrentProcess = file["currentProcess"];
        Json::Value jsonGeneratedJSONPath = file["generatedJSONPath"];
        Json::Value jsonFailFiles = file["failFiles"];

        if ( jsonQtyFiles != Json::nullValue ) {
            filesFound = jsonQtyFiles.asInt();
        }
        if ( jsonProgress != Json::nullValue ) {
            progress = jsonProgress.asFloat();
        }
        if ( jsonCurrentProcess != Json::nullValue ) {
            currentProcess = jsonCurrentProcess.asString() + "...";
        }
        if ( jsonGeneratedJSONPath != Json::nullValue ) {
            generatedJSONPath = jsonGeneratedJSONPath.asString();
        }
        if ( jsonFailFiles != Json::nullValue ) {
            failFiles.clear();
            for ( unsigned int i = 0 ; i < jsonFailFiles.size() ; i++ ) {
                failFiles.push_back( jsonFailFiles[i].asString() );
            }
        }

    } else {
        currentProcess = "Initializing...";
    }
}

void AudioDimensionalityReduction::end()
{
    ofFilePath ofp;
    string processInfoPath;
    
    #ifdef TARGET_OS_MAC
    processInfoPath = ofp.join( ofp.getCurrentExeDir() + "../Resources", FILENAME_PROCESS_INFO );
    #else
    processInfoPath = ofp.join( ofp.getCurrentExeDir(), FILENAME_PROCESS_INFO);
    #endif

    ofFile::removeFile( processInfoPath );

    currentProcess = "";
    progress = 0;
    filesFound = 0;
    generatedJSONPath = "";
    failFiles.clear();
}

void AudioDimensionalityReduction::threadedFunction()
{
    if ( isThreadRunning() ) {
        run(dirPath,
            targetSampleRate,
            targetDuration,
            audioFeatures,
            stft_windowSize,
            stft_hopSize,
            pca_nComponents,
            tsne_perplexity,
            tsne_theta);
    }
}
