#pragma once

#include "ofMain.h"

namespace ColorPaletteGenerator {
    static vector<int> colors = {0xa6cee3,
                                 0x1f78b4,
//                                 0xb2df8a,
                                 0x0991af,
//                                 0x33a02c,
                                 0x4325af,
                                 0xfb9a99,
                                 0x9e1213,
                                 0xfdbf6f,
                                 0xff7f00,
                                 0xcab2d6,
                                 0xa58ac2,
                                 0xffff99,
//                                 0xb15928
                                };

    static int originalColorCount = colors.size();
    static ofColor stripeMainColor =  ofColor(0x2e2e2e);
    static const ofColor noiseColor =  ofColor(0xc7c7c7);

    static void setCantColors(int cant) {
//        int atC = 0;

//        //Fallback por si hay más cluster que colores
//        while(cant > colors.size()) {
//            ofColor c = ofColor::fromHex(colors[atC]);
//            c.setSaturation(c.getSaturation()*0.6);
//            c.setBrightness(c.getBrightness()*0.6);
//            colors.push_back(c.getHex());
//            atC++;
//        }
        while(cant > colors.size()) {

            int randomIndex1 = ceil(ofRandom(0,colors.size()-2));
            int randomIndex2;
            do {
                randomIndex2 = ceil(ofRandom(0,colors.size()-2));
            } while( randomIndex2 == randomIndex1 );

            ofColor c = ofColor::fromHex(colors[ randomIndex1 ]);
            c.lerp( colors[ randomIndex2 ], ofRandom(0.3,0.8) );

            colors.push_back(c.getHex());
        }
    }

    static ofColor getColor(int idx){
//        if ( idx >= 0 ) {
//            return ofColor::fromHex(colors[idx]);
//        } else {
//            return noiseColor;
//        }
        if ( idx < 0 ) {
            return noiseColor;
        }
        return ofColor::fromHex( colors[ idx % originalColorCount ] );
    }

    static ofColor getRandomOriginalColor() {
        return ofColor::fromHex( colors[ ceil(ofRandom(0, originalColorCount)) ] );
    }
};
