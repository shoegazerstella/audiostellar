#include "ofApp.h"
//--------------------------------------------------------------
void ofApp::setup() {
    #ifdef TARGET_OS_MAC
        ofSetDataPathRoot("../Resources/data/");
    #endif
    
    ofSetWindowTitle("AudioStellar");
    ofSetEscapeQuitsApp(false);

    ofEnableAlphaBlending();
    ofSetVerticalSync(true);

    sounds = Sounds::getInstance();
    oscServer = OscServer::getInstance();
    midiServer = new MidiServer();
    modes = new Modes(midiServer);
    sessionManager = new SessionManager(modes, midiServer);
    gui = Gui::getInstance(midiServer,  modes, sessionManager, oscServer);

    midiServer->init(modes);
    cam.init();

    //check if json passed from command line
    #ifndef TARGET_OS_MAC
    if(arguments.size() > 1){
        SessionManager::datasetLoadOptions opts;
        opts.method = "TERMINAL";
        opts.path = arguments.at(1);
        sessionManager->loadSession(opts);
    }
    #endif

    if ( !sessionManager->getSessionLoaded() ) {
        sessionManager->loadDefaultSession();
    }

}

//--------------------------------------------------------------
void ofApp::update() {
    sounds->update();
    modes->update();
    oscServer->update();
}

//--------------------------------------------------------------
void ofApp::draw() {
    ofBackground(25);

    if ( useCam ) {
        cam.begin();
    }

    modes->beforeDraw();
    sounds->draw();
    modes->draw();

    if ( useCam ) {
        cam.end();
    }

    gui->draw();
}

void ofApp::keyPressed(ofKeyEventArgs & e) {
    gui->keyPressed(e);
    modes->keyPressed(e);
    sessionManager->keyPressed(e);
}

void ofApp::mousePressed(int x, int y, int button) {
    if(!gui->isMouseHoveringGUI()){
        ofVec3f realCoordinates = cam.screenToWorld(ofVec3f(x,y,0));
        modes->mousePressed( realCoordinates.x,
                             realCoordinates.y,
                             button );

        sounds->mousePressed( realCoordinates.x,
                              realCoordinates.y,
                              button);
    }
}

void ofApp::mouseMoved(int x, int y) {
    if(sessionManager->getSessionLoaded()){
        ofVec3f realCoordinates = cam.screenToWorld(ofVec3f(x,y,0));
        if(!gui->isMouseHoveringGUI()){
            sounds->mouseMoved(realCoordinates.x,
                               realCoordinates.y,
                               false);
        }
    }
}

void ofApp::mouseDragged(int x, int y, int button) {
    if(!gui->isMouseHoveringGUI()){
        ofVec3f realCoordinates = cam.screenToWorld(ofVec3f(x,y,0));
        modes->mouseDragged(realCoordinates.x,
                            realCoordinates.y,
                            button);

        sounds->mouseDragged(realCoordinates, button);
    }

}

void ofApp::mouseReleased(int x, int y, int button) {
    if(!gui->isMouseHoveringGUI()){
        ofVec3f realCoordinates = cam.screenToWorld(ofVec3f(x,y,0));
        modes->mouseReleased(realCoordinates.x,
                             realCoordinates.y,
                             button);
    }
}

void ofApp::keyReleased(int key) {
    if ( key == 'c' ) {
        useCam = !useCam;
    }

}
void ofApp::mouseScrolled(int x, int y, float scrollX, float scrollY) {
    // cam.mouseScrolled(x,y,scrollX,scrollY);
}

void ofApp::exit(){
    sessionManager->exit();
    ofLogNotice("EXIT");
}



