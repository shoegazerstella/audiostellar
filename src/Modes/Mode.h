#ifndef _MODE
#define _MODE

#include "ofMain.h"
#include "Utils.h"
#include "ofxJSON.h"

class Mode {
private:

public:
    string modeName = "";

    string iconPath = "";
    string iconPathActive = "";

    //Mode(){};
    virtual void beforeDraw() {}
    virtual void draw() {}
    virtual void drawGui(){}
    virtual void update() {}

    virtual string getModeName() {
        return modeName;
    };

    virtual void mousePressed(ofVec2f p, int button) {};
    virtual void mouseDragged(ofVec2f p, int button){};
    virtual void mouseReleased(ofVec2f p){};
    virtual void keyPressed(ofKeyEventArgs & e) {};

    virtual void onSelectedMode(){};
    virtual void onUnselectedMode(){};

    virtual void mouseMoved(ofVec2f p){};
    virtual void midiMessage(Utils::midiMsg m){};

    virtual void setupGui(){};

    virtual Json::Value save(){ return Json::nullValue; };
    virtual void load( Json::Value jsonData ){};

    virtual bool isMouseHoveringGui(){ return false; };
    
    virtual void reset(){};
    
    ~Mode() {};

};
#endif
