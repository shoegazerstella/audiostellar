.. AudioStellar documentation master file, created by
   sphinx-quickstart on Fri Oct 19 15:39:07 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to AudioStellar's user's manual!
========================================

.. toctree::
    :maxdepth: 1
    :caption: Basics
    :name: sec-Basics

    basics/index
    basics/ui

.. toctree::
    :maxdepth: 2
    :caption: Modes
    :name: sec-Modes

    modes/index



.. Indices and tables
.. ==================
..
.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
